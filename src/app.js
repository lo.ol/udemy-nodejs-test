const path = require('path')
const express = require('express')
const hbs = require('hbs')
const forcast = require('./forcast')


const app = express()
const port = process.env.PORT || 3000

// define paths to express config
const publicDirPath = path.join(__dirname, '../public')

//define views path
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

//setup static dir
app.use(express.static(publicDirPath))
hbs.registerPartials(partialsPath)

app.set('view engine', 'hbs')
app.set('views', viewsPath)

app.get('', (req,res)=>{
    res.render('index', {title: 'Weather App', name: "alin"})
})

app.get('/about', (req,res)=>{
    res.render('about', {title: 'About me', name: "alin"})
})

app.get('/help', (req,res)=>{
    res.render('help', {msg:"Help me", title: 'help', name:'alin'})
})

app.get('/weather', (req, res)=>{
    if(!req.query.address){
        return res.send({
            error:'You must provide an address'
        })
    }
    forcast.forecast(req.query.address, (error, data)=>{
        if(error){
            return res.send({
                'error': error
            })
        }
        data['location'] =  req.query.address
        res.send(data)
        
    })
    
})



app.get('/help/*',(req,res)=>{
    res.render('404', {msg: "Help article not found", title: '404', name: 'alin'})

})
app.get('*',(req,res)=>{
    res.render('404', {msg: "Page not found", title: '404', name: 'alin'})

})


app.listen(port, ()=>{
    console.log('Server is up on port 3000')
})