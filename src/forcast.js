const request = require('request')


  //Geocoding
const forecast = (latitude, callback)=>{
    const url = 'http://api.weatherstack.com/current?access_key=5c32cca8084983c3b68faeace8acd048&query='+latitude
    console.log(url)
    request({url, json: true}, (error, {body})=>{
        if(error){
           callback("Unable to connect to weather service!")
        }else if(body.error){
            callback('Unable to find location')
        }   
        
        else{

            const current = body.current
            callback(undefined, {weather_descriptions: current.weather_descriptions[0],
                                temperature: current.temperature,
                            feelslike: current.feelslike})
        }
    })
}

forecast(location = process.argv[2], (error, data) => {
    console.log('Error', error)
    console.log('Data', data)
    return data
})

module.exports = {'forecast' : forecast}